deploy-vps:
	rsync -avhzL --delete \
				--no-perms --no-owner --no-group \
				--exclude .git \
				--filter=":- .gitignore" \
				. root@103.97.124.29:/home/node_server/light_localsanbay/
	ssh root@103.97.124.29 "cd /home/node_server/light_localsanbay && pm2 restart 3"