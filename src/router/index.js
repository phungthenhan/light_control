import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const router = new Router({
  // mode: 'history',
  routes: [
    // Each of these routes are loaded asynchronously, when a user first navigates to each corresponding endpoint.
    // The route will load once into memory, the first time it's called, and no more on future calls.
    // This behavior can be observed on the network tab of your browser dev tools.
    {
      path: '/login',
      name: 'login',
      component: function (resolve) {
        require(['@/components/login/Login.vue'], resolve)
      }
    },
    {
      path: '/',
      name: 'login',
      component: function (resolve) {
        require(['@/components/login/Login.vue'], resolve)
      }
    },
    {
      path: '/signup',
      name: 'signup',
      component: function (resolve) {
        require(['@/components/signup/Signup.vue'], resolve)
      }
    },
    {
      path: '/control_group',
      name: 'control',
      component: function (resolve) {
        require(['@/components/control/Control.vue'], resolve)
      },
      beforeEnter: guardRoute

    },
    {
      path: '/control_scription',
      name: 'controlscription',
      component: function (resolve) {
        require(['@/components/script/Scription.vue'], resolve)
      },
      beforeEnter: guardRoute

    },
    {
      path: '/control_group/:id',
      name: 'detailcontrol',
      component: function (resolve) {
        require(['@/components/control/detailcontrol/DetailControl.vue'], resolve)
      },
      beforeEnter: guardRoute

    },
    {
      path: '/checktest',
      name: 'checktest',
      component: function (resolve) {
        require(['@/components/control/CheckTest.vue'], resolve)
      },
      beforeEnter: guardRoute
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: function (resolve) {
        require(['@/components/dashboard/Dashboard.vue'], resolve)
      },
      beforeEnter: guardRoute
    },
    {
      path: '/device',
      name: 'device',
      component: function (resolve) {
        require(['@/components/dashboard/Device.vue'], resolve)
      },
      beforeEnter: guardRoute
    },
    {
      path: '/group',
      name: 'group',
      component: function (resolve) {
        require(['@/components/dashboard/Group.vue'], resolve)
      },
      beforeEnter: guardRoute
    },
    {
      path: '/scription',
      name: 'scription',
      component: function (resolve) {
        require(['@/components/dashboard/Scription.vue'], resolve)
      },
      beforeEnter: guardRoute
    },
    {
      path: '/user',
      name: 'user',
      component: function (resolve) {
        require(['@/components/dashboard/User.vue'], resolve)
      },
      beforeEnter: guardRoute
    },
    {
      path: '/script/:id',
      name: 'detailscript',
      component: function (resolve) {
        require(['@/components/script/detailscript/DetailScript.vue'], resolve)
      },
      beforeEnter: guardRoute

    }
  ]
})

function guardRoute (to, from, next) {
  // work-around to get to the Vuex store (as of Vue 2.0)
  const auth = router.app.$options.store.state.auth
  const checkrole = checkRole(to)
  if (checkrole) {
    if (!auth.isLoggedIn) {
      next({
        path: '/login',
        query: { redirect: to.fullPath }
      })
    } else {
      next()
    }
  } else {
    next({
      path: from.path,
      query: { redirect: to.fullPath }
    })
  }
}

function checkRole (to) {
  var routername = ['dashboard', 'group', 'scription', 'device', 'user']
  const user = router.app.$options.store.state.user
  if (routername.includes(to.name) && user.userinfo.role !== 'Manager' && !user.userinfo.admin) {
    return false
  } else {
    return true
  }
}
export default router
